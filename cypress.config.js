const { defineConfig } = require("cypress");

module.exports = defineConfig({
  
  env: {
    Link: '/cypress/e2e/E-Wallet/LINK AJA/LinkV1.html',
    Link2: 'cypress/e2e/E-Wallet/LINK AJA/LinkV2.html',
    
  },
  e2e: {
    setupNodeEvents(on, config) {
      
    },
    "experimentalSessionAndOrigin": true,
    "chromeWebSecurity": false,
    "supportFile" : false

  },
});
